// Tomas Daniel Nieto #2034643
package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

import linear_algebra.Vector3d;

public class Vector3dTest {

    @Test
    public void testGetX() {
        Vector3d vector3d = new Vector3d(1, 2, 3);
        assertEquals(1, vector3d.getX());
    }

    @Test
    public void testGetY() {
        Vector3d vector3d = new Vector3d(1, 2, 3);
        assertEquals(2, vector3d.getY());
    }

    @Test
    public void testGetZ() {
        Vector3d vector3d = new Vector3d(1, 2, 3);
        assertEquals(3, vector3d.getZ());
    }

    @Test
    public void testMagnitude() {
        Vector3d vector3d = new Vector3d(-3, 1, 3);
        assertEquals(4.358898943540674, vector3d.magnitude());
    }

    @Test
    public void testDotProduct() {
        Vector3d vector3d = new Vector3d(3, 2, 1);
        Vector3d vector3d2 = new Vector3d(2, 1, 0);
        assertEquals(8, vector3d.dotProduct(vector3d2));
    }

    @Test
    public void add() {
        Vector3d vector3d = new Vector3d(3, 2, 1);
        Vector3d vector3d2 = new Vector3d(2, 1, 0);
        Vector3d resultant = vector3d.add(vector3d2);
        assertEquals(5, resultant.getX());
        assertEquals(3, resultant.getY());
        assertEquals(1, resultant.getZ());
    }
}
