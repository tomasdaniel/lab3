// Tomas Daniel Nieto #2034643
package linear_algebra;

public class Vector3d {
    private final double x, y, z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double magnitude() {
        return Math.sqrt(x*x + y*y + z*z);
    }

   public double dotProduct(Vector3d vector3d) {
        return this.x * vector3d.getX() + this.y * vector3d.getY() + this.z * vector3d.getZ();
    }

    public Vector3d add(Vector3d vector3d) {
        return new Vector3d(this.x + vector3d.getX(), this.y + vector3d.getY(), this.z + vector3d.getZ());
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }
}